package com.example.querydsl.demo;

import static java.util.Arrays.asList;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    ApplicationRunner cargadorDePersonas(PersonaRepository personaRepository) {
        return (a) -> {
            personaRepository.save(asList(
                    new Persona("Jorge", 18, "37485759", true),
                    new Persona("Pablo", 40, "32385750", false),
                    new Persona("Fede", 36, "35885751", false),
                    new Persona("Marker", 23, "37385757", true),
                    new Persona("Jorge", 12, "34685775", true),
                    new Persona("David", 10, "34685775", true),
                    new Persona("Josecito", 30, "37685759", false)
            ));
        };
    }
}
