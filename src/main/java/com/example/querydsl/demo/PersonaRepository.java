package com.example.querydsl.demo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

public interface PersonaRepository extends JpaRepository<Persona, Long>, QueryDslPredicateExecutor<Persona> {

}
