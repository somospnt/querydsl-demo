package com.example.querydsl.demo;

import com.querydsl.core.types.Predicate;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PersonaController {

    private final PersonaRepository personaRepository;

    public PersonaController(PersonaRepository personaRepository) {
        this.personaRepository = personaRepository;
    }

    @GetMapping("/personas")
    public Iterable<Persona> buscar(@QuerydslPredicate Predicate predicate) {
        return personaRepository.findAll(predicate);
    }

    @GetMapping("/personas/mayoresDe18")
    public Iterable<Persona> buscarMayoresDe18() {
        return personaRepository.findAll(QPersona.persona.edad.goe(18));
    }
}
