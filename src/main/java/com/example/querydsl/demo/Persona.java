package com.example.querydsl.demo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Persona {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String nombre;
    private Integer edad;
    private String dni;
    private boolean esBuenTipo;

    public Persona() {
    }

    public Persona(String nombre, Integer edad, String dni, boolean esBuenTipo) {
        this.nombre = nombre;
        this.edad = edad;
        this.dni = dni;
        this.esBuenTipo = esBuenTipo;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getEdad() {
        return edad;
    }

    public void setEdad(Integer edad) {
        this.edad = edad;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public boolean isEsBuenTipo() {
        return esBuenTipo;
    }

    public void setEsBuenTipo(boolean esBuenTipo) {
        this.esBuenTipo = esBuenTipo;
    }
}
